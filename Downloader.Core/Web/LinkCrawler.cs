﻿using System;
using System.Collections.Generic;
using System.IO;

using Downloader.Core.Collections;
using Downloader.Core.Model;
using Downloader.Core.Validation.Concrete;
using HtmlAgilityPack;

namespace Downloader.Core.Web
{
     public class LinkCrawler
     {
          private readonly Uri websiteUri;
          private readonly string baseAddress;
          private readonly string websiteAddress;
          private readonly LinkCrawlerHelper linkHelper;

          public LinkCrawler( string website )
          {
               websiteUri = new Uri( website );
               baseAddress = websiteUri.Scheme + "://" + websiteUri.Host + "/";
               websiteAddress = website;
               linkHelper = new LinkCrawlerHelper( baseAddress, websiteAddress );
          }

          public DownloadableFileCollection FindDownloadableFiles()
          {
               var downloadableFileCollection = new DownloadableFileCollection();
               var linkedFiles = GetLinkedFiles();
               foreach ( var file in linkedFiles )
               {
                    downloadableFileCollection.Add( file );
               }
               return downloadableFileCollection;
          }

          private List<DownloadableFile> GetLinkedFiles()
          {
               var anchorNodes = GetAllAnchorNodesWithHrefValue();
               var downloadableFiles = new List<DownloadableFile>();
               if ( anchorNodes != null )
               {
                    downloadableFiles = BuildDownloadableFilesFromAnchorNodes( anchorNodes );
               }
               return downloadableFiles;
          }

          private IEnumerable<HtmlNode> GetAllAnchorNodesWithHrefValue()
          {
               var doc = GetHtmlDocument();
               var anchorNodes = doc.DocumentNode.SelectNodes( "//a[@href]" );
               return anchorNodes;
          }

          private HtmlDocument GetHtmlDocument()
          {
               var web = new HtmlWeb();
               return web.Load( websiteAddress );
          }

          private List<DownloadableFile> BuildDownloadableFilesFromAnchorNodes( IEnumerable<HtmlNode> anchorNodes )
          {
               var downloadableFiles = new List<DownloadableFile>();
               var filesAlreadyInList = new List<String>();
               var fileValidator = new FileValidator();
               foreach ( var node in anchorNodes )
               {
                    var hrefAttribute = node.Attributes [ "href" ].Value;
                    if ( fileValidator.IsValid( hrefAttribute ) && !filesAlreadyInList.Contains( hrefAttribute ) )
                    {
                         filesAlreadyInList.Add( hrefAttribute );
                         var foundFile = BuildDownlodableFileFromHrefAttribute( hrefAttribute );
                         downloadableFiles.Add( foundFile );
                    }
               }

               return downloadableFiles;
          }

          private DownloadableFile BuildDownlodableFileFromHrefAttribute( string hrefAttribute )
          {
               var downloadableFile = new DownloadableFile();
               downloadableFile.DownloadLink = linkHelper.IsRelativeHref( hrefAttribute ) ? linkHelper.GetRemainingMissingPath( hrefAttribute ) : hrefAttribute;
               downloadableFile.FileName = Path.GetFileNameWithoutExtension( hrefAttribute );
               downloadableFile.FileType = Path.GetExtension( hrefAttribute );
               return downloadableFile;
          }
     }
}
