﻿using System;
using System.IO;

namespace Downloader.Core.Web
{
	public class LinkCrawlerHelper
	{

		private string baseAddress;
		private string websiteAddress;
		public LinkCrawlerHelper(string baseAddress, string websiteAddress)
		{
			this.baseAddress = baseAddress;
			this.websiteAddress = websiteAddress;
		}

		public Boolean IsRelativeHref(string href)
		{
			return !href.Contains(baseAddress);
		}

		public String GetRemainingMissingPath( String href )
		{
			var temporaryAddress = websiteAddress;
			if ( temporaryAddress.EndsWith( ".html" ) )
			{
				temporaryAddress = temporaryAddress.Replace( ".html", "/" );
			}
			var baseAddressEndIndex = baseAddress.Length;
			var missingPathLength = temporaryAddress.Length - baseAddressEndIndex;
			var missingPath = "";
			if ( missingPathLength > 0 )
				missingPath = temporaryAddress.Substring( baseAddressEndIndex, missingPathLength );
			var fileName = Path.GetFileName( href );
			return baseAddress + missingPath + fileName;
		}
	}
}
