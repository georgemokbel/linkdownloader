﻿using System;
using System.Collections.Generic;
using System.Linq;

using Downloader.Core.Model;

namespace Downloader.Core.Collections
{
	public class DownloadableFileCollection
	{
		private Dictionary<String, List<DownloadableFile>> downloadableFiles = new Dictionary<string, List<DownloadableFile>>( );

		public Dictionary<String, List<DownloadableFile>> DownloadableFiles { get { return downloadableFiles; } }

		public List<DownloadableFile> this [ string fileType ]
		{
			get { return downloadableFiles [ fileType ]; }
		}

		public void Add( DownloadableFile file )
		{
			try
			{
				downloadableFiles [ file.FileType ].Add( file );
			}
			catch
			{
				downloadableFiles.Add( file.FileType, new List<DownloadableFile>( ) );

				downloadableFiles [ file.FileType ].Add( file );
			}
		}

		public int GetNumberOfFilesForType( string fileType )
		{
			try
			{
				return downloadableFiles [ fileType ].Count;
			}
			catch
			{
				return 0;
			}
		}

		public int GetTotalNumberOfFiles( )
		{
			var totalNumberOfFiles = 0;
			totalNumberOfFiles += downloadableFiles.Sum( fileType => fileType.Value.Count );
			return totalNumberOfFiles;
		}


	}
}
