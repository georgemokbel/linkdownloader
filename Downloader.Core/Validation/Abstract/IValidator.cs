﻿using System;

namespace Downloader.Core.Validation.Abstract
{
	public interface IValidator
	{
		bool IsValid(String target);
	}
}
