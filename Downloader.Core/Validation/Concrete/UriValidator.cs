﻿using System;
using Downloader.Core.Validation.Abstract;

namespace Downloader.Core.Validation.Concrete
{
	public class UriValidator : IValidator
	{
		public bool IsValid(string target)
		{
			Uri targetAsValidUri;
			if ( Uri.TryCreate( target, UriKind.Absolute, out targetAsValidUri ) )
			{
				return IsValidScheme( targetAsValidUri );
			}
			return false;
		}

		private bool IsValidScheme( Uri uri )
		{
			return uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps;
		}
	}
}
