﻿using System;
using System.Net;
using Downloader.Core.Validation.Abstract;

namespace Downloader.Core.Validation.Concrete
{
	public class WebsiteValidator : IValidator
	{
		public bool IsValid( string target )
		{

			var request = CreateWebRequest( target );
			if ( request != null )
			{
				return GetRequestResponseStatusCode( request ) == HttpStatusCode.OK;
			}
			return false;
		}

		private HttpWebRequest CreateWebRequest( String target )
		{
			try
			{
				var request = ( HttpWebRequest ) WebRequest.Create( target );
				request.AllowAutoRedirect = false;
				request.Method = "HEAD";
				request.Timeout = 3500;
				return request;
			}
			catch
			{
				return null;
			}
		}

		private HttpStatusCode GetRequestResponseStatusCode( HttpWebRequest request )
		{
			try
			{
				var response = ( HttpWebResponse ) request.GetResponse( );
				return response.StatusCode;
			}
			catch
			{
				return HttpStatusCode.BadRequest;
			}
		}
	}
}
