﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Downloader.Core.Validation.Abstract;

namespace Downloader.Core.Validation.Concrete
{
	public class FileValidator : IValidator
	{
		private readonly string[] webBlackList = new string[]
		{
			".com",
			".edu",
			".net",
			".org",
			".tv",
			".tk",
			".int",
			".gov",
			".mil",
			".asia",
			"mailto:",
			"&amp",
			"%20",
		};

		private readonly string[] extensionFileBlackList = new string[]
		{
			".php",
			".php3",
			".php4",
			".js",
			".jsp",
			".cfm",
			".asp",
			".aspx",
			".axd",
			".asx",
			".css",
			".cfm",
			".htm",
			".html",
			".zhtml",
			".atom",
			".rss"
		};

		public bool IsValid(string target)
		{
			var fileName = Path.GetFileName(target);
			var extension = Path.GetExtension(fileName);
			if (!String.IsNullOrEmpty(fileName) && !String.IsNullOrEmpty(extension))
				return IsValidFileName(fileName) && IsValidFileExtension(extension);
			return false;
		}

		private Boolean IsValidFileName(string target)
		{
			return webBlackList.All(blackListed => !target.Contains(blackListed));
		}

		private Boolean IsValidFileExtension(string target)
		{
			return extensionFileBlackList.All(badExtension => !target.EndsWith(badExtension));
		}
	}
}
