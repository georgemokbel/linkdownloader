﻿
namespace Downloader.Core.Model
{
	public class DownloadableFile
	{
		public string FileType { get; set; }

		public string FileName { get; set; }

		public string DownloadLink { get; set; }
	}
}
