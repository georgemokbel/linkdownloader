﻿using Downloader.Core.Model;
using System;
using System.Configuration;

namespace Downloader.Core.Settings
{
     public static class AppSettings
     {
          public static ApplicationSettings GetApplicationSettings( )
          {
               if ( Properties.Settings.Default.UpgradeRequired )
               {
                    Properties.Settings.Default.Upgrade( );
                    Properties.Settings.Default.UpgradeRequired = false;
               }
               var settings = new ApplicationSettings( );
               settings.SaveFilePath = Properties.Settings.Default.SaveFilePath;
               return settings;
          }

          public static void RefreshSettings( )
          {
               Properties.Settings.Default.Reload( );
          }

          public static String GetSetting( String key )
          {
               try
               {
                    var setting = Properties.Settings.Default[ key ];
                    return ( String ) setting;
               }
               catch ( SettingsPropertyNotFoundException e )
               {
                    return e.Message;
               }
          }

          public static void SetSetting( String key, String value )
          {
               try
               {
                    Properties.Settings.Default [ key ] = value;
                    Properties.Settings.Default.UpgradeRequired = true;
                    Properties.Settings.Default.Save();
               }
               catch ( SettingsPropertyNotFoundException e )
               {

               }
          }
     }
}
