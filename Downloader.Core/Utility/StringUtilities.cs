﻿using System;

namespace Downloader.Core.Utility
{
     public static class StringUtilities
     {
          public static String PrependMissingProtocolToString( String badString )
          {
               if (badString.StartsWith("http://") || badString.StartsWith("https://"))
               {
                    return badString;
               }
               return "http://" + badString;
          }
     }
}
