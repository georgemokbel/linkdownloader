﻿using System;
using System.Collections.Generic;
using Downloader.Core.Model;

namespace Downloader.UI.Model
{
	public class DownloadableFileComboBoxItem
	{
		public string FileType { get; set; }
		public int NumberOfFiles { get; set; }
		public string DisplayString { get { return String.Format("{0} ({1} file(s))", FileType, NumberOfFiles); } }
		public List<DownloadableFile> DownloadableFiles = new List<DownloadableFile>(); 
	}
}
