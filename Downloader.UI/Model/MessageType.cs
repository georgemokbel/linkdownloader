﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Downloader.UI.Model
{
     public enum MessageType
     {
          SettingsMessage,
          LogMessage,
          HelpMessage,
          AboutMessage,
          WindowMessage,
     }

     public enum SettingsMessageType
     {
          OpenWindow,
          CloseWindow,
          SaveSettings
     }
}
