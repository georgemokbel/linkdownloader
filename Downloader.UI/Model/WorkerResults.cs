﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Downloader.UI.Model
{
     enum CrawlerWorkerResults
     {
          SUCCESS,
          NO_FILES_FOUND,
          WEBSITE_ACCESS_ERROR
     }

     enum DownloadWorkerResults
     {
          SUCCESS,
          FAILED
     }
}
