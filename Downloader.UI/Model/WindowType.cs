﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Downloader.UI.Model
{
     public enum WindowType
     {
          SettingsWindow,
          LogWindow,
          GeneralHelpWindow,
          AdvancedHelpWindow,
          AboutWindow
     }
}
