﻿using System;
using System.Windows;

using Downloader.Core.Model;
using Downloader.Core.Settings;
using Downloader.UI.Service.Abstract;

using GalaSoft.MvvmLight.Ioc;

namespace Downloader.UI
{
     /// <summary>
     /// Interaction logic for App.xaml
     /// </summary>
     public partial class App : Application
     {
          public static IWindowService WindowService
          {
               get { return SimpleIoc.Default.GetInstance< IWindowService >( ); }
          }

	     public static ApplicationSettings AppSettings
	     {
			 get { return Core.Settings.AppSettings.GetApplicationSettings(); }
	     }

          internal void Application_Startup( object sender, StartupEventArgs e )
          {
               if ( !IsApplicationConfigured() )
               {
	               ShowStartUpConfigurationMessage();
               }
			   StartupUri = new Uri( "/Downloader.UI;component/MainWindow.xaml", UriKind.Relative );
          }


          internal void ShowStartUpConfigurationMessage()
          {
               MessageBox.Show( "Settings are not configured. Please verify them in the 'SETTINGS' tab." );
          }

          internal bool IsApplicationConfigured( )
          {
               if ( String.IsNullOrEmpty( AppSettings.SaveFilePath ))
                    return false;
               return true;
          }
     }
}
