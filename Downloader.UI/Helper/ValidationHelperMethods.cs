﻿using Downloader.Core.Utility;
using Downloader.Core.Validation.Concrete;
using System;

namespace Downloader.UI.Helper
{
     public static class ValidationHelperMethods
     {
          public static Boolean IsValidTargetWebAddress( String targetWebAddress )
          {
               var uriValidator = new UriValidator( );
               var websiteValidator = new WebsiteValidator( );
               if ( uriValidator.IsValid( targetWebAddress ) && websiteValidator.IsValid( targetWebAddress ) )
               {
                    return true;
               }
               return false;
          }
     }
}
