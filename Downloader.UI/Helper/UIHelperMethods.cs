﻿using Downloader.Core.Collections;
using Downloader.UI.Model;
using System.Collections.Generic;

namespace Downloader.UI.Helper
{
     public static class UIHelperMethods
     {
          public static List< DownloadableFileComboBoxItem > GetComboBoxItems(
               DownloadableFileCollection downloadableFiles )
          {
               var comboBoxItems = new List< DownloadableFileComboBoxItem >( );
               foreach ( var fileType in downloadableFiles.DownloadableFiles )
               {
                    var comboBoxItem = new DownloadableFileComboBoxItem( );
                    comboBoxItem.FileType = fileType.Key;
                    comboBoxItem.NumberOfFiles = downloadableFiles.GetNumberOfFilesForType( fileType.Key );
                    comboBoxItem.DownloadableFiles = fileType.Value;
                    comboBoxItems.Add( comboBoxItem );
               }
               return comboBoxItems;
          } 
     }
}
