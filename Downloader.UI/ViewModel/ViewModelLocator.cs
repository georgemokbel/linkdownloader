using Downloader.UI.Service.Abstract;
using Downloader.UI.Service.Concrete;
using Downloader.UI.View;

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace Downloader.UI.ViewModel
{
	public class ViewModelLocator
	{
		public ViewModelLocator( )
		{
			ServiceLocator.SetLocatorProvider( ( ) => SimpleIoc.Default );
			SimpleIoc.Default.Register<IWindowService, WindowService>();
			SimpleIoc.Default.Register<MenuStripViewModel>( );
			SimpleIoc.Default.Register<SettingsViewModel>( );
			SimpleIoc.Default.Register<MainViewModel>( );
			SimpleIoc.Default.Register<AboutViewModel>( );
		}

		public MainViewModel Main
		{
			get
			{
				return ServiceLocator.Current.GetInstance<MainViewModel>( );
			}
		}

		public MenuStripViewModel MenuStrip
		{
			get
			{
				return ServiceLocator.Current.GetInstance<MenuStripViewModel>( );
			}
		}

		public SettingsViewModel Settings
		{
			get
			{
				return ServiceLocator.Current.GetInstance<SettingsViewModel>( );
			}
		}

		public AboutViewModel About
		{
			get { return ServiceLocator.Current.GetInstance<AboutViewModel>(); }
		}

		public static void Cleanup( )
		{
			// TODO Clear the ViewModels
		}
	}
}