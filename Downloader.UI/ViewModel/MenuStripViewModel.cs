﻿using System.Windows.Input;

using Downloader.UI.Model;

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;


namespace Downloader.UI.ViewModel
{
     public class MenuStripViewModel : ViewModelBase
     {

          public ICommand ShowWindowCommand { get; private set; }

          public MenuStripViewModel( )
          {
               ShowWindowCommand = new RelayCommand< WindowType >( ShowWindow_Execute );
          }

          private void ShowWindow_Execute( WindowType windowType )
          {
               App.WindowService.ShowWindow( windowType );
          }
     }
}
