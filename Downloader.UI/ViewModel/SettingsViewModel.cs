﻿using System;
using Downloader.Core.Settings;

using GalaSoft.MvvmLight;

namespace Downloader.UI.ViewModel
{
     public class SettingsViewModel : ViewModelBase
     {
	     private string saveFilePath = App.AppSettings.SaveFilePath;

	     public string SaveFilePath
	     {
		     get { return saveFilePath; }
		     set
		     {
				 AppSettings.SetSetting("SaveFilePath", value);
			     saveFilePath = value;
			     RaisePropertyChanged("SaveFilePath");
		     }
	     }

          public SettingsViewModel( )
          {
	          if (String.IsNullOrEmpty(saveFilePath))
	          {
		          SaveFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
	          }
          }

     }
}
