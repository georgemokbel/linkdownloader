using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Documents;
using Downloader.UI.Model;
using GalaSoft.MvvmLight;

namespace Downloader.UI.ViewModel
{
	public partial class MainViewModel : ViewModelBase
	{
		private static readonly MenuStripViewModel menuStripViewModel = new MenuStripViewModel( );
		private string targetWebAddress = "";
		private Boolean findFilesButtonEnabled;
		private Boolean downloadFilesButtonEnabled;
		private Boolean cancelDownloadFilesButtonEnabled;
		private int progressBarValue = 0;
		private List<DownloadableFileComboBoxItem> downloadableFileComboBoxItems = new List<DownloadableFileComboBoxItem>();
		private DownloadableFileComboBoxItem selectedComboBoxItem = new DownloadableFileComboBoxItem();
		private readonly BackgroundWorker downloadWorker = new BackgroundWorker();
		private readonly BackgroundWorker crawlerWorker = new BackgroundWorker();

		public String TargetWebAddress
		{
			get { return targetWebAddress; }
			set
			{
				if ( !String.IsNullOrEmpty( value ) )
				{
					FindFilesButtonEnabled = true;
				}
				else
				{
					FindFilesButtonEnabled = false;
				}
				targetWebAddress = value;
				RaisePropertyChanged( "TargetWebAddress" );

			}
		}
		public Boolean CancelDownloadFilesButtonEnabled
		{
			get { return cancelDownloadFilesButtonEnabled; }
			set
			{
				cancelDownloadFilesButtonEnabled = value;
				RaisePropertyChanged( "CancelDownloadFilesButtonEnabled" );
			}
		}

		public Boolean FindFilesButtonEnabled
		{
			get { return findFilesButtonEnabled; }
			set
			{
				findFilesButtonEnabled = value;
				RaisePropertyChanged( "FindFilesButtonEnabled" );
			}
		}

		public Boolean DownloadFilesButtonEnabled
		{
			get { return downloadFilesButtonEnabled; }
			set
			{
				downloadFilesButtonEnabled = value;
				RaisePropertyChanged( "DownloadFilesButtonEnabled" );
			}
		}


		public int ProgressBarValue
		{
			get
			{
				return progressBarValue;
			}
			set
			{
				progressBarValue = value;
				RaisePropertyChanged( "ProgressBarValue" );
			}
		}

		public ViewModelBase MenuStripViewModel
		{
			get { return menuStripViewModel; }
		}

		public List<DownloadableFileComboBoxItem> DownloadableFileComboBoxItems
		{
			get
			{
				return downloadableFileComboBoxItems;

			}
			set
			{
				downloadableFileComboBoxItems = value;
				RaisePropertyChanged("DownloadableFileComboBoxItems");
			}
		}

		public DownloadableFileComboBoxItem SelectedComboBoxItem
		{
			get { return selectedComboBoxItem; }
			set
			{
				selectedComboBoxItem = value;
				RaisePropertyChanged("SelectedComboBoxItem");
			}
		}

	     public MainViewModel( )
	     {
	          SetUpCommands( );
	          SetUpBackgroundWorkers( );
	     }
	}
}