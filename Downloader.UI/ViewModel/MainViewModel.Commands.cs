﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows;

using Downloader.Core.Utility;
using Downloader.Core.Web;
using Downloader.UI.Helper;
using Downloader.UI.Model;

using GalaSoft.MvvmLight.Command;
using System.Windows.Input;

namespace Downloader.UI.ViewModel
{
     public partial class MainViewModel
     {
          public ICommand FindFilesCommand { get; private set; }
          public ICommand DownloadFilesCommand { get; private set; }

          private void FindFiles_Execute()
          {
               crawlerWorker.RunWorkerAsync();
          }

          private void DownloadFiles_Execute()
          {
               downloadWorker.RunWorkerAsync();
          }

          private void SetUpCommands()
          {
               FindFilesCommand = new RelayCommand( FindFiles_Execute );
               DownloadFilesCommand = new RelayCommand( DownloadFiles_Execute );
          }

          private void SetUpBackgroundWorkers()
          {
               crawlerWorker.DoWork += CrawlerWorkerOnDoWork;
               crawlerWorker.RunWorkerCompleted += CrawlerWorkerOnRunWorkerCompleted;

               downloadWorker.WorkerSupportsCancellation = true;
               downloadWorker.WorkerReportsProgress = true;
               downloadWorker.DoWork += DownloadWorkerOnDoWork;
               downloadWorker.ProgressChanged += DownloadWorkerOnProgressChanged;
               downloadWorker.RunWorkerCompleted += DownloadWorkerOnRunWorkerCompleted;
          }

          


          #region Download Worker Functions

          private void DownloadWorkerOnDoWork( object sender, DoWorkEventArgs doWorkEventArgs )
          {
               DisableButtonsForDownloadWorker( );

               using ( var client = new WebClient( ) )
               {
                    var files = selectedComboBoxItem.DownloadableFiles;
                    var maxProgress = files.Count;
                    var savePath = App.AppSettings.SaveFilePath;
                    for ( var i = 0; i < files.Count; ++i )
                    {
                         if ( downloadWorker.CancellationPending )
                              return;
                         var fullSavePath = savePath + "\\" + files [ i ].FileName + "." +
                                            files[ i ].FileType;
                         try
                         {
                              client.DownloadFile( files[ i ].DownloadLink, fullSavePath );
                         }
                         catch
                         {
                              // not logging or reporting errors at the moment.
                         }
                         downloadWorker.ReportProgress(Convert.ToInt32( ((decimal)i/(decimal)maxProgress) * 100));
                    }
               }
          }

          private void DownloadWorkerOnProgressChanged( object sender, ProgressChangedEventArgs progressChangedEventArgs )
          {
               ProgressBarValue = progressChangedEventArgs.ProgressPercentage;
          }

          private void DownloadWorkerOnRunWorkerCompleted( object sender,
                                                           RunWorkerCompletedEventArgs runWorkerCompletedEventArgs )
          {
               ProgressBarValue = 100;
               EnableButtonsForDownloadWorker( );
               ProgressBarValue = 0;
          }

          private void DisableButtonsForDownloadWorker( )
          {
               FindFilesButtonEnabled = false;
               DownloadFilesButtonEnabled = false;
               CancelDownloadFilesButtonEnabled = true;
          }

          private void EnableButtonsForDownloadWorker( )
          {
               DownloadFilesButtonEnabled = true;
               CancelDownloadFilesButtonEnabled = false;
               FindFilesButtonEnabled = true;
          }
          #endregion

          #region Crawler Worker Functions 

          private void CrawlerWorkerOnDoWork( object sender, DoWorkEventArgs doWorkEventArgs )
          {
               DisableButtonsForCrawlerWorker();
               TargetWebAddress = StringUtilities.PrependMissingProtocolToString( TargetWebAddress );
               if ( ValidationHelperMethods.IsValidTargetWebAddress( TargetWebAddress ) )
               {
                    var crawler = new LinkCrawler( TargetWebAddress );
                    var downloadableFileCollection = crawler.FindDownloadableFiles( );
                    DownloadableFileComboBoxItems = UIHelperMethods.GetComboBoxItems( downloadableFileCollection );
                    if ( DownloadableFileComboBoxItems.Count > 0 )
                    {
                         SelectedComboBoxItem = DownloadableFileComboBoxItems.First( );
                         doWorkEventArgs.Result = CrawlerWorkerResults.SUCCESS;
                    }
                    else
                    {
                         doWorkEventArgs.Result = CrawlerWorkerResults.NO_FILES_FOUND;
                    }
               }
               else
               {
                    doWorkEventArgs.Result = CrawlerWorkerResults.WEBSITE_ACCESS_ERROR;
               }
          }

          private void CrawlerWorkerOnRunWorkerCompleted( object sender,
                                                          RunWorkerCompletedEventArgs runWorkerCompletedEventArgs )
          {
               var result = ( CrawlerWorkerResults )runWorkerCompletedEventArgs.Result;
               EnableButtonsForCrawlerWorker( result );
               if ( result == CrawlerWorkerResults.NO_FILES_FOUND )
                    MessageBox.Show( "No downloadable files were found at the specified website." );
               else if ( result == CrawlerWorkerResults.WEBSITE_ACCESS_ERROR )
                    MessageBox.Show( "There was an error trying to access the specified website." );  
          }

          private void DisableButtonsForCrawlerWorker( )
          {
               FindFilesButtonEnabled = false;
               DownloadFilesButtonEnabled = false;
          }

          private void EnableButtonsForCrawlerWorker( CrawlerWorkerResults result )
          {
               FindFilesButtonEnabled = true;
               CancelDownloadFilesButtonEnabled = false;
               DownloadFilesButtonEnabled = (result) == CrawlerWorkerResults.SUCCESS;
          }

          #endregion
     }
}
