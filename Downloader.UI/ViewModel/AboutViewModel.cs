﻿using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Downloader.UI.ViewModel
{
	public class AboutViewModel : ViewModelBase
	{
		public ICommand NavigateToGitHubCommand { get; private set; }
		public ICommand NavigateToGeorgeMokbelCommand { get; private set; }

		public AboutViewModel( )
		{
			NavigateToGitHubCommand = new RelayCommand( NavigateToGitHub_Execute );
			NavigateToGeorgeMokbelCommand = new RelayCommand( NavigateToGeorgeMokbel_Execute );
		}

		private void NavigateToGitHub_Execute( )
		{
			System.Diagnostics.Process.Start( "http://www.github.com/gmokbel/LinkDownloader" );
		}

		private void NavigateToGeorgeMokbel_Execute( )
		{
			System.Diagnostics.Process.Start( "http://www.georgemokbel.com" );
		}
	}
}
