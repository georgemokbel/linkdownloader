﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Downloader.UI.Model;

namespace Downloader.UI.Service.Abstract
{
     public interface IWindowService
     {
          void ShowWindow( WindowType windowType );
     }
}
