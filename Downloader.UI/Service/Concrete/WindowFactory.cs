﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using Downloader.UI.Model;
using Downloader.UI.View;

namespace Downloader.UI.Service.Concrete
{
     public class WindowFactory
     {
          public Window CreateWindow( WindowType windowType )
          {
               var window = new Window( );  
               switch ( windowType )
               {
                    case WindowType.AboutWindow:
                         window = new AboutView( );
                         break;
                    case WindowType.SettingsWindow:
                         window = new SettingsView( );
                         break;
               }
               window.Owner = App.Current.MainWindow;
               window.WindowStyle = WindowStyle.ToolWindow;
               window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
               return window;
          }
     }
}
