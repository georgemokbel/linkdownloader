﻿
using System.Net.Mime;

using Downloader.UI.Model;
using Downloader.UI.Service.Abstract;

namespace Downloader.UI.Service.Concrete
{
     public class WindowService : IWindowService
     {
          public void ShowWindow( WindowType windowType )
          {
               var windowFactory = new WindowFactory( );
               var window = windowFactory.CreateWindow( windowType );
               window.ShowDialog( );
          }
     }
}
