﻿using System.Windows.Controls;
using System.Windows.Input;

namespace Downloader.UI.View
{
     /// <summary>
     /// Interaction logic for Menu.xaml
     /// </summary>
     public partial class MenuStrip : UserControl
     {
          public MenuStrip()
          {
               InitializeComponent();
          }

          private void MenuItem_MouseEnter( object sender, MouseEventArgs e )
          {
               var menuItem = ( MenuItem )sender;
               if ( menuItem.HasItems && menuItem.IsSubmenuOpen == false)
               {
                    menuItem.IsEnabled = false;
                    menuItem.IsSubmenuOpen = true;
               }
          }

          private void MenuItem_MouseLeave( object sender, MouseEventArgs e )
          {
               var menuItem = ( MenuItem )sender;
               if ( !menuItem.IsEnabled )
               {
                    menuItem.IsEnabled = true;
               }
          }
     }
}
