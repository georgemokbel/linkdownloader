﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UserControl = System.Windows.Controls.UserControl;

namespace Downloader.UI.Control
{
	/// <summary>
	/// Interaction logic for SelectFolderControl.xaml
	/// </summary>
	public partial class SelectFolderControl : UserControl
	{
		public SelectFolderControl( )
		{
			InitializeComponent( );
		}

		public static DependencyProperty TextProperty = DependencyProperty.Register( "Text", typeof( string ), typeof( SelectFolderControl ), new FrameworkPropertyMetadata( null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault ) );
		public static DependencyProperty DescriptionProperty = DependencyProperty.Register( "Description", typeof( string ), typeof( SelectFolderControl ), new PropertyMetadata( null ) );

		public string Text { get { return GetValue( TextProperty ) as string; } set { SetValue( TextProperty, value ); } }

		public string Description { get { return GetValue( DescriptionProperty ) as string; } set { SetValue( DescriptionProperty, value ); } }

		private void BrowseFolder( object sender, RoutedEventArgs e )
		{
			using ( var folderBrowserDialog = new FolderBrowserDialog( ) )
			{
				folderBrowserDialog.Description = Description;
				folderBrowserDialog.SelectedPath = Text;
				folderBrowserDialog.ShowNewFolderButton = true;
				DialogResult result = folderBrowserDialog.ShowDialog( );
				if ( result == System.Windows.Forms.DialogResult.OK )
				{
					Text = folderBrowserDialog.SelectedPath;
					BindingExpression be = GetBindingExpression( TextProperty );
					if ( be != null )
						be.UpdateSource( );
				}
			}
		}
	}
}
